import './App.css'
import Pizza from './Pizza'

function App() {
  return (
    <>
      <h1>Hello React!</h1>
      <Pizza />
    </>
  )
}

export default App
